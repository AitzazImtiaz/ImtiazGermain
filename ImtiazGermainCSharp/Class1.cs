﻿namespace ImtiazGermainCSharp
{
    public class ImtiazGermain
    {
        public bool PrimeCheck(int n)
        {
            if (n == 2 || n == 3)
                return true;
            if (n <= 1 || n % 2 == 0 || n % 3 == 0)
                return false;
            int i = 5;

            while (i * i <= n)
            {
                if (n % i == 0 || n % (i + 2) == 0)
                    return false;
                i += 6;
            }

            return true;
        }
        public bool IsGermain(int a)
        {
            int b;
            b = (2 * a) + 1;
            if (PrimeCheck(a) == true & PrimeCheck(b) == true)
                return true;
            else
                return false;
        }
        public bool IsImtiazGermain(int a)
        {
            int b, c, d;
            b = (2 * a) + 1;
            c = (2 * b) + 1;
            d = (2 * c) + 1;
            if (PrimeCheck(a) == true & PrimeCheck(b) == true & PrimeCheck(c) == false & PrimeCheck(d) == true)
                return true;
            else
                return false;
        }
        public int Integrate(int a, int b)
        {
            int result;
            result = (int)(((Math.Pow(a, 2)) + a) - ((Math.Pow(b, 2)) + b));
            return result;
        }
    }

}