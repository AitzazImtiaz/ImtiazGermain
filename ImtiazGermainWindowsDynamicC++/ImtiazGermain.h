#pragma once
#ifdef IMTIAZGERMAIN_EXPORTS
#define IMTIAZGERMAIN_API __declspec(dllexport)
#else
#define IMTIAZGERMAIN_API __declspec(dllimport)
#endif

extern "C" IMTIAZGERMAIN_API bool PrimeCheck(
    unsigned long long a);
extern "C" IMTIAZGERMAIN_API bool IsGermain(
    unsigned long long a);
extern "C" IMTIAZGERMAIN_API bool IsImtiazGermain(
    unsigned long long a);
extern "C" IMTIAZGERMAIN_API unsigned long long Integrate(
    unsigned long long a, unsigned long long b);