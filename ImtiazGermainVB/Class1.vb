Public Class Germain
    Public Function PrimeCheck(ByVal n As Integer) As Boolean
        If n = 2 OrElse n = 3 Then Return True
        If n <= 1 OrElse n Mod 2 = 0 OrElse n Mod 3 = 0 Then Return False
        Dim i As Integer = 5

        While i * i <= n
            If n Mod i = 0 OrElse n Mod (i + 2) = 0 Then Return False
            i += 6
        End While

        Return True
    End Function
    Public Function IsGermain(ByVal a As Integer) As Boolean
        Dim b As Integer
        b = (2 * a) + 1
        If PrimeCheck(a) = True And PrimeCheck(b) = True Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function IsImtiazGermain(ByVal a As Integer) As Boolean
        Dim b, c, d As Integer
        b = (2 * a) + 1
        c = (2 * b) + 1
        d = (2 * c) + 1
        If PrimeCheck(a) = True And PrimeCheck(b) = True And PrimeCheck(c) = False And PrimeCheck(d) = True Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function Integrate(ByVal a As Integer, ByVal b As Integer)
        Dim result As Integer
        result = ((a ^ 2) + a) - ((b ^ 2) + b)
        Return result
    End Function
End Class