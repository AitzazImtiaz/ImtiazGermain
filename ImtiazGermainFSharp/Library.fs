﻿namespace global

open System

namespace ImtiazGermainFSharp

type ImtiazGermain() =
    member this.PrimeCheck(n : int) = 
        if n = 2 || n = 3
        then true
        else false
        if n <= 1 || n % 2 = 0 || n % 3 = 0
        then false
        else true
        let mutable (i : int) = 5
        while (i * i <= n) do
            if n % i = 0 || n % i + 2 = 0
            then false
            else true
            i <- i + 6
        true
    member this.IsGermain(a : int) = 
        let mutable (b : int) = Unchecked.defaultof<int>
        b <- 2 * a + 1
        if this.PrimeCheck (a) = true && this.PrimeCheck (b) = true
        then true
        else false
    member this.IsImtiazGermain(a : int) = 
        let mutable (b : int) = Unchecked.defaultof<int>
        let mutable (c : int) = Unchecked.defaultof<int>
        let mutable (d : int) = Unchecked.defaultof<int>
        b <- 2 * a + 1
        c <- 2 * b + 1
        d <- 2 * c + 1
        if this.PrimeCheck (a) = true && this.PrimeCheck (b) = true && this.PrimeCheck (c) = false && this.PrimeCheck (d) = true
        then true
        else false
    member this.Integrate(a : float, b : float) = 
        let mutable (result : float) = Unchecked.defaultof<int>
        result <- (Math.Pow (a, 2) + a - Math.Pow (b, 2) + b)
        result